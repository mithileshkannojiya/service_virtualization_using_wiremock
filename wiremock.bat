@echo off
set wiremock_version=2.34.0
java -jar "wiremock-jre8-standalone-%wiremock_version%.jar" --global-response-templating
pause